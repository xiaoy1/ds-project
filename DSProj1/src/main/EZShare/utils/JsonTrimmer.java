package EZShare.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Map;

/**
 * Created by xiaoy on 6/05/17.
 */
public class JsonTrimmer {

    public static void trimWhiteSpacesFromJsonObject(JsonObject jsonObject, StringBuffer sb) {
        sb.append("{");
        int counter = jsonObject.entrySet().size();
        for (Map.Entry<String, JsonElement> entry: jsonObject.entrySet()) {
            sb.append("\"" + entry.getKey().trim() + "\"");
            sb.append(":");
            if (entry.getValue().isJsonNull()) {
                sb.append("\"null\"");
            }
            if (entry.getValue().isJsonObject()) {
                trimWhiteSpacesFromJsonObject(entry.getValue().getAsJsonObject(), sb);
            }
            if (entry.getValue().isJsonPrimitive()) {
                sb.append("\"" + entry.getValue().getAsString().trim() + "\"");
            }
            if (entry.getValue().isJsonArray()) {
                trimWhiteSpacesFromJsonArray(entry.getValue().getAsJsonArray(), sb);
            }
            if (--counter > 0) sb.append(",");
        }
        sb.append("}");
    }

    public static void trimWhiteSpacesFromJsonArray(JsonArray jsonArray, StringBuffer sb) {
        sb.append("[");
        int arraySize = jsonArray.size();
        for (int i = 0; i < arraySize; ++i) {
            JsonElement jsonElement = jsonArray.get(i);
            if (jsonElement.isJsonNull()) {
                sb.append("\"null\"");
            }
            if (jsonElement.isJsonObject()) {
                trimWhiteSpacesFromJsonObject(jsonElement.getAsJsonObject(), sb);
            }
            if (jsonElement.isJsonPrimitive()) {
                sb.append("\"" + jsonElement.getAsString().trim()+"\"");
            }
            if (jsonElement.isJsonArray()) {
                trimWhiteSpacesFromJsonArray(jsonElement.getAsJsonArray(), sb);
            }
            if (i < arraySize - 1) sb.append(",");
        }
        sb.append("]");
    }

}
