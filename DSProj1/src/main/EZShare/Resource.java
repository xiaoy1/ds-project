package EZShare;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Resource {

	private String name;
	private String description;
	private String uri;
	private String channel;
	private String owner;
	private String ezserver;
	private List<String> tags;
	private Long resourceSize;

	public Resource(){
		name = description = uri = channel = owner = ezserver = "";
		tags = new ArrayList<>();
	}

	public Resource(Resource resource){
		setName(resource.getName());
		setDescription(resource.getDescription());
		setUri(resource.getUri());
		setChannel(resource.getChannel());
		setOwner(resource.getOwner());
		setEzserver(resource.getEzserver());
		setTags(resource.getTags());
		setResourceSize(resource.getResourceSize());
	}

	public static boolean matchTags(List<String> templateTags, List<String> candidateTags) {

		// templateTags is null (or empty), which is a subset of any other set
		if (templateTags == null || templateTags.size() == 0) return true;

		if (candidateTags == null || candidateTags.size() == 0) return false;

		return !CollectionUtils.intersection(templateTags.stream().map(String::toLowerCase).collect(Collectors.toList()), candidateTags.stream().map(String::toLowerCase).collect(Collectors.toList())).isEmpty();

	}

	public void setDefaultValues() {
		if (this.name == null) name = "";
		if (this.description == null) description = "";
		if (this.uri == null) uri = "";
		if (this.channel == null) channel = "";
		if (this.owner == null) owner = "";
		if (this.ezserver == null) ezserver = "";
		if (this.tags == null) tags = new ArrayList<>();
	}

	public boolean isEmpty() {
		if ("".equals(name) &&
				"".equals(description) &&
				"".equals(uri) &&
				"".equals(channel) &&
				"".equals(owner) &&
				"".equals(ezserver) &&
				tags.size() == 0) {
			return true;
		}
		return false;
	}

	public boolean compareUriAndChannel(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Resource resource = (Resource) o;

		if (uri != null ? !uri.equals(resource.uri) : resource.uri != null)
			return false;
		return channel != null ? channel.equals(resource.channel) : resource.channel == null;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getUri() {
		return uri;
	}
	public void setUri(String uRI) {
		uri = uRI;
	}

	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getEzserver() {
		return ezserver;
	}
	public void setEzserver(String eZserver) {
		ezserver = eZserver;
	}

	public List<String> getTags() {
		return tags;
	}
	public void setTags(String...strings) {   //numbers of parameters are uncertain
		tags = new ArrayList<>();
		for(String temp: strings){
			tags.add(temp);
		}
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Long getResourceSize() {
		return resourceSize;
	}

	public void setResourceSize(Long resourceSize) {
		this.resourceSize = resourceSize;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Resource resource = (Resource) o;

		if (uri != null ? !uri.equals(resource.uri) : resource.uri != null)
			return false;
		if (channel != null ? !channel.equals(resource.channel) : resource.channel != null)
			return false;
		return owner != null ? owner.equals(resource.owner) : resource.owner == null;
	}

	@Override
	public int hashCode() {
		int result = uri != null ? uri.hashCode() : 0;
		result = 31 * result + (channel != null ? channel.hashCode() : 0);
		result = 31 * result + (owner != null ? owner.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Resource{" +
				"name='" + name + '\'' +
				", description='" + description + '\'' +
				", uri='" + uri + '\'' +
				", channel='" + channel + '\'' +
				", owner='" + owner + '\'' +
				", ezserver='" + ezserver + '\'' +
				", tags=" + tags +
				", resourceSize=" + resourceSize +
				'}';
	}

}
