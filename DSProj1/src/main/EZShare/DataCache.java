package EZShare;

import EZShare.subscription.SubscriptionRegistry;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Stream;

/**
 *
 * Created by xiaoy on 15/05/17.
 */
public class DataCache implements Iterable<Resource> {

    private CopyOnWriteArraySet<Resource> resources;

    private SubscriptionRegistry registry;

    public DataCache(SubscriptionRegistry registry) {
        this.registry = registry;
        resources = new CopyOnWriteArraySet<>();
    }

    public void add(Resource resource) throws IOException {
        resources.remove(resource);
        resources.add(resource);
        registry.addJob(new Resource(resource));
    }

    public boolean remove(Resource resource) {
        return resources.remove(resource);
    }

    public boolean contains(Resource resoure) {
        return resources.contains(resoure);
    }

    @Override public Iterator<Resource> iterator() {
        return resources.iterator();
    }

    public Stream<Resource> stream() {
        return resources.stream();
    }

    public SubscriptionRegistry getRegistry() {
        return registry;
    }

}
