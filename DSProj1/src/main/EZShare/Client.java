/**
 * 
 */
package EZShare;

import EZShare.messaging.*;
import EZShare.subscription.DataInputStreamEventHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.Socket;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class Client {

	protected static final int UNIX_ENTER_KEY_CODE = 10;
	protected static final int WINDOWS_ENTER_KEY_CODE = 13;

	protected static final Logger logger = LogManager.getRootLogger();
	protected static final Gson gson = new GsonBuilder().create();

	private SSLSocketFactory sslSocketFactory = null;
	private Socket socket = null;

	private String channel;
	private String description;
	private String host;
	private String name;
	private int portNumber;
	private String secret;
	private String servers;
	private String tags[];
	private String uri;
	private String owner;
	private String id;

	private Resource createResource() {
		Resource resource = new Resource();
		resource.setName(this.name);
		resource.setTags(this.tags);
		resource.setDescription(this.description);
		resource.setUri(this.uri);
		resource.setChannel(this.channel);
		resource.setOwner(this.owner);
		resource.setEzserver(null);
		return resource;
	}

	private static Client instance;

	private Client(){}

	public static Client getInstance(){
		if(instance == null){
			synchronized (Client.class) {
				if(instance == null){
					instance = new Client();
				}
			}
		}
		return instance;
	}

	protected void sendMessage(String message, DataOutputStream out) {
        try {
			out.writeUTF(message);
			out.flush();
			logger.debug("SENT: {}", message);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	protected void publishCommand() {

		PublishCommand publishCommand = new PublishCommand();
		publishCommand.setResource(createResource());

        try (
        		Socket socket = this.socket;
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				DataInputStream in = new DataInputStream(socket.getInputStream())
		){
            logger.info("publishing to {}:{}", host, portNumber);
            sendMessage(gson.toJson(publishCommand), out);
			logger.trace("Socket::isConnected(): {}", socket.isConnected());
			String line = null;
			while ((line = in.readUTF()) != null) {
				logger.debug("RECEIVED: {}", line);
			}
        } catch (IOException e) {
			// suppress EOFException exception
			if (!(e instanceof EOFException)) {
				logger.error(e);
			}
		}

	}

	protected void removeCommand() {

		RemoveCommand removeCommand = new RemoveCommand();
		removeCommand.setResource(createResource());

		try (
				Socket socket = this.socket;
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				DataInputStream in = new DataInputStream(socket.getInputStream())
		) {
			logger.info("removing from {}:{}", host, portNumber);
			sendMessage(gson.toJson(removeCommand), out);
			logger.trace("Socket::isConnected(): {}", socket.isConnected());
			String line = null;
			while ((line = in.readUTF()) != null) {
				logger.debug("RECEIVED: {}", line);
			}
		} catch (IOException e) {
			// suppress EOFException exception
			if (!(e instanceof EOFException)) {
				logger.error(e);
			}
		}
	}

	protected void shareCommand() {

		ShareCommand shareCommand = new ShareCommand();
		shareCommand.setResource(createResource());
		shareCommand.setSecret(secret);

		try (
				Socket socket = this.socket;
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				DataInputStream in = new DataInputStream(socket.getInputStream())
		) {
			logger.info("sharing to {}:{}", host, portNumber);
			sendMessage(gson.toJson(shareCommand), out);
			logger.trace("Socket::isConnected(): {}", socket.isConnected());
			String line = null;
			while ((line = in.readUTF())!= null){
				logger.debug("RECEIVED:{}", line);
			}

		}catch (IOException e){
			if(!(e instanceof EOFException)){
				logger.error(e);
			}
		}

	}


	protected void queryCommand() {

		QueryCommand queryCommand = new QueryCommand();
		queryCommand.setRelay(true);
		queryCommand.setResourceTemplate(createResource());

		try (
				Socket socket = this.socket;
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				DataInputStream in = new DataInputStream(socket.getInputStream())
		) {
			logger.info("querying from {}:{}", host, portNumber);
			sendMessage(gson.toJson(queryCommand), out);
			logger.trace("Socket::isConnected(): {}", socket.isConnected());
			String line = null;
			while ((line = in.readUTF()) != null) {
				logger.debug("RECEIVED: {}", line);
			}
		} catch (IOException e) {
			// suppress EOFException exception
			if (!(e instanceof EOFException)) {
				logger.error(e);
			}
        }

    }

    protected void fetchCommand() {

		FetchCommand fetchCommand = new FetchCommand();
		fetchCommand.setResourceTemplate(createResource());

		try (
				Socket socket = this.socket;
			 	DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			 	DataInputStream in = new DataInputStream(socket.getInputStream())
		) {
			logger.info("fetch from {}:{}", host, portNumber);
			sendMessage(gson.toJson(fetchCommand), out);
			logger.trace("Socket::isConnected(): {}", socket.isConnected());
			String line = in.readUTF();
			logger.debug("RECEIVED: {}", line);
			AcknowledgeTemplate ack = gson.fromJson(line, AcknowledgeTemplate.class);
			if ("success".equals(ack.getResponse().toLowerCase())) {

				line = in.readUTF();
				JsonObject jsonObject = new JsonParser().parse(line).getAsJsonObject();
			    if (jsonObject.has("resultSize")) {
					logger.debug("RECEIVED: {}", line);
					return;
				}
				logger.debug("RECEIVED: {}", line);
				Resource resource = gson.fromJson(jsonObject, Resource.class);

				String[] temp = resource.getUri().split("/");
				String fileName = temp[temp.length-1];
				logger.trace("Fetched file name: {}", fileName);
				logger.trace("Fetched file size: {}", resource.getResourceSize());

				OutputStream outputStream = new FileOutputStream(Paths.get(fileName).toString());
				logger.trace("Download path: {}", Paths.get(fileName).toString());

				int bufferSize = socket.getReceiveBufferSize();
				byte[] buffer = new byte[bufferSize];
				long bytesRemaining = resource.getResourceSize();
				int bytesRead = -1;
				while (bytesRemaining > 0 &&
						(bytesRead = in.read(buffer, 0, (int) Math.min(bufferSize, bytesRemaining))) != -1) {
					outputStream.write(buffer, 0, bytesRead);
					bytesRemaining -= bytesRead;
				}
				outputStream.close();

				// read last confirmation message
				logger.debug("RECEIVED: {}", in.readUTF());

			}

		} catch (IOException e) {
			// suppress EOFException exception
			if (!(e instanceof EOFException)) {
				logger.error(e);
			}
		}

	}

	protected void exchangeCommand() {

		ExchangeCommand exchangeCommand = new ExchangeCommand();
		List<ServerTemplate> serverTemplates = new ArrayList<>();
		for (String serverAndPort : this.servers.split(",")) {
			String temp[] = serverAndPort.split(":");
			ServerTemplate serverTemplate = new ServerTemplate(temp[0], Integer.parseInt(temp[1]));
			serverTemplates.add(serverTemplate);
		}
		exchangeCommand.setServerList(serverTemplates);

		try (
				Socket socket = this.socket;
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				DataInputStream in = new DataInputStream(socket.getInputStream())
		) {
			logger.info("exchanging from {}:{}", host, portNumber);
			sendMessage(gson.toJson(exchangeCommand), out);
			logger.trace("Socket::isConnected(): {}", socket.isConnected());
			String line = null;
			while ((line = in.readUTF()) != null) {
				logger.debug("RECEIVED: {}", line);
			}
		} catch (IOException e) {
			// suppress EOFException exception
			if (!(e instanceof EOFException)) {
				logger.error(e);
			}
		}

	}

	protected void subscribeCommand() {

		SubscribeCommand subscribeCommand = new SubscribeCommand();
		subscribeCommand.setRelay(true);
		subscribeCommand.setResourceTemplate(createResource());
		subscribeCommand.setId(id);

		try (
				Socket socket = this.socket;
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				DataInputStream in = new DataInputStream(socket.getInputStream())
		) {

			logger.info("subscribing from {}:{}", host, portNumber);
			sendMessage(gson.toJson(subscribeCommand), out);
			logger.trace("Socket::isConnected(): {}", socket.isConnected());

			String line = null;
			BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));

			DataInputStreamEventHandler inputStreamHandler = new DataInputStreamEventHandler(in);
			Thread handlerThread = new Thread(inputStreamHandler);
			handlerThread.start();

			boolean isStopped = false;
			while(!isStopped) {
				if (bufferReader.ready()) {
					int key = bufferReader.read();
					if (key == UNIX_ENTER_KEY_CODE || key == WINDOWS_ENTER_KEY_CODE) {
						isStopped = true;
						UnsubscribeCommand unsubscribeCommand = new UnsubscribeCommand();
                        unsubscribeCommand.setId(subscribeCommand.getId());
						logger.info("Unsubscribing from {}:{}", host, portNumber);
						sendMessage(gson.toJson(unsubscribeCommand), out);
                        logger.trace("Socket::isConnected(): {}", socket.isConnected());
						continue;
					}
				}
				if (inputStreamHandler.available()) {
					logger.info("RECEIVED: {}", inputStreamHandler.get());
				}
			}

			while(true) {
				if (inputStreamHandler.available()) {
					String msg = inputStreamHandler.get();
					logger.info("RECEIVED: {}", msg);
					if (msg.contains("resultSize"))
						break;
				}
			}

		} catch (IOException e) {
			// suppress EOFException exception
			if (!(e instanceof EOFException)) {
				logger.error(e);
			}
		} catch (InterruptedException e) {
			logger.error(e);
		}

	}

	public static void main(String[] args) {

		Client client = getInstance();

		try{
			
			Options option = new Options();
			option.addOption("channel",true,"channel");
			option.addOption("debug",false,"print debug information");
			option.addOption("description",true,"resourceTemplate description");
			option.addOption("exchange",false,"exchange server list with server");
			option.addOption("fetch",false,"resourceTemplate");
			option.addOption("host",true,"server host, a domain name of IP subscriberAddress");
			option.addOption("name",true,"resourceTemplate name");
			option.addOption("owner",true,"owner");
			option.addOption("port",true,"server port, an integer");
			option.addOption("publish",false,"publish resourceTemplate on server");
			option.addOption("query",false,"query for resources on server");
			option.addOption("remove",false,"remove resourceTemplate form server");
			option.addOption("secret",true,"secret");
			option.addOption("servers",true,"server list, host1:port1,host2:post2,...");
			option.addOption("share",false,"share resourceTemplate on server");
			option.addOption("tags",true,"resourceTemplate tags, tag1,tag2,tag3,...");
			option.addOption("uri",true,"resourceTemplate URI");
			option.addOption("h", "help",  false, "print help for the command");
			option.addOption("secure",false,"make a secure connection");
			option.addOption("subscribe",false,"subscribe for resources on server");
			option.addOption("id",true,"subscription id specified by the client");
			
			// create the command line parser  
		    CommandLineParser parser = new DefaultParser();  
		    CommandLine cmd = parser.parse(option, args); 
		    
		    // check the options have been set correctly 
			if ((cmd.hasOption("h")||(cmd.hasOption("help")))){
			    System.out.println("-------------------------------------------------------------");
			    HelpFormatter formatter = new HelpFormatter();  
			    formatter.printHelp( "Client", option ); 
			    System.out.println("-------------------------------------------------------------");
		    }

			Level level = null;
			if (cmd.hasOption("debug")){
				level = Level.DEBUG;
				logger.info("setting debug on");
			} else {
				level = Level.INFO;
			}

//            level = Level.TRACE;

			LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
			Configuration conf = ctx.getConfiguration();
			conf.getLoggerConfig(LogManager.ROOT_LOGGER_NAME).setLevel(level);
			ctx.updateLoggers(conf);

			if (cmd.hasOption("host")){
				client.setHost(cmd.getOptionValue("host"));
			} else {
				client.setHost("localhost");
			}

			if (cmd.hasOption("port")){
				client.setPortNumber(Integer.parseInt(cmd.getOptionValue("port")));
			} else if (cmd.hasOption("secure")) {
				client.setPortNumber(Server.DEFAULT_SECURE_PORT_NUMBER);
			} else {
				client.setPortNumber(Server.DEFAULT_PORT_NUMBER);
			}

			try {
				if (cmd.hasOption("secure")){

					InputStream keystoreInput = Thread.currentThread().getContextClassLoader()
							.getResourceAsStream("keystores/clientkeystore");
					byte[] buffer = new byte[keystoreInput.available()];
					keystoreInput.read(buffer);

					File targetFile = new File("clientkeystore");
					OutputStream outStream = new FileOutputStream(targetFile);
					outStream.write(buffer);

					System.setProperty("javax.net.ssl.trustStore", "clientkeystore");
					client.setSslSocketFactory((SSLSocketFactory) SSLSocketFactory.getDefault());
					client.setSocket((SSLSocket) client.getSslSocketFactory().createSocket(client.getHost(), client.getPortNumber()));

				} else {
					client.setSocket(new Socket(client.getHost(), client.getPortNumber()));
				}

			} catch (IOException e) {
				// suppress EOFException exception
				if (!(e instanceof EOFException)) {
					logger.error(e);
				}
			}

			if (cmd.hasOption("uri")){
				client.setUri(cmd.getOptionValue("uri"));
			} else {
                client.setUri("");
            }

		    if (cmd.hasOption("channel")){
		    	client.setChannel(cmd.getOptionValue("channel"));
		    } else {
				client.setChannel("");
			}

		    if (cmd.hasOption("description")){
				client.setDescription(cmd.getOptionValue("description"));
		    } else {
				client.setDescription("");
			}

		    if (cmd.hasOption("name")){
		    	client.setName(cmd.getOptionValue("name"));
		    } else {
				client.setName("");
			}

		    if (cmd.hasOption("owner")){
		    	client.setOwner(cmd.getOptionValue("owner"));
			} else {
				client.setOwner("");
			}

			if (cmd.hasOption("tags")){
				String tags = cmd.getOptionValue("tags");
				client.setTags(tags.split(","));
			} else {
				client.setTags(new String[0]);
			}

		    if (cmd.hasOption("secret")){
		    	client.setSecret(cmd.getOptionValue("secret"));
		    }

		    if (cmd.hasOption("servers")){
		    	client.setServers(cmd.getOptionValue("servers"));
		    }

			if (cmd.hasOption("fetch")){
				client.fetchCommand();
			}

			if (cmd.hasOption("publish")){
                client.publishCommand();
			}

			if (cmd.hasOption("share")){
				 client.shareCommand();
			}

			if (cmd.hasOption("query")){
                client.queryCommand();
			}

			if (cmd.hasOption("remove")){
				client.removeCommand();
			}

			if (cmd.hasOption("exchange")){
				client.exchangeCommand();
			}

			if (cmd.hasOption("subscribe")){
			    if (cmd.hasOption("id")) {
                    client.id = cmd.getOptionValue("id");
                }
				client.subscribeCommand();
			}

		}catch(Exception e){
			System.err.println("Unexpected exception:"+e.getMessage());
		}

	}

	public SSLSocketFactory getSslSocketFactory() {
		return sslSocketFactory;
	}

	public void setSslSocketFactory(SSLSocketFactory sslSocketFactory) {
		this.sslSocketFactory = sslSocketFactory;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getServers() {
		return servers;
	}

	public void setServers(String servers) {
		this.servers = servers;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tag) {
		this.tags = tag;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

}
