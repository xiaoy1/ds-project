package EZShare.messaging;

/**
 * Created by xiaoy on 30/03/17.
 */
public abstract class AbstractCommand {

    protected String command;

    public String getCommand() {
        return command;
    }

}
