package EZShare.messaging;

/**
 * Created by shaw on 17/4/18.
 */
public class Result {
    private int resultSize;

    public int getResultSize() {
        return resultSize;
    }

    public void setResultSize(int resultSize) {
        this.resultSize = resultSize;
    }

    public Result(int resultSize) {
        this.resultSize = resultSize;
    }
}
