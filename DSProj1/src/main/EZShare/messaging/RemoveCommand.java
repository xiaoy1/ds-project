package EZShare.messaging;

import EZShare.Resource;

/**
 * Created by shaw on 17/3/30.
 */
public class RemoveCommand extends AbstractCommand{

    protected Resource resource;

    public RemoveCommand() {
        this.command = "REMOVE";
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }
}
