package EZShare.messaging;

/**
 * Created by Chong Feng on 2017/3/30.
 */

import EZShare.Resource;

public class PublishCommand extends AbstractCommand {

    public PublishCommand() {
        this.command = "PUBLISH";
    }

    private Resource resource = new Resource();

    public Resource getResource() {return resource;}
    public void setResource(Resource aResource) {resource = aResource;}
}