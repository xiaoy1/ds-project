package EZShare.messaging;

/**
 *
 * id is not used
 *
 * Created by xiaoy on 18/05/17.
 */
public class UnsubscribeCommand extends AbstractCommand {

    protected String id;

    public UnsubscribeCommand() {
        this.command = "UNSUBSCRIBE";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
