package EZShare.messaging;

/**
 * Created by xiaoy on 31/03/17.
 */
public class AcknowledgeTemplate {

    protected String response;

    protected String errorMessage;

    protected String id;

    public AcknowledgeTemplate(String response, String errorMessage) {
        this.response = response;
        this.errorMessage = errorMessage;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
