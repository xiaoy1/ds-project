package EZShare.messaging;

import EZShare.Resource;

/**
 *
 * id is not used
 *
 * Created by xiaoy on 16/05/17.
 */
public class SubscribeCommand extends AbstractCommand {

    protected Resource resourceTemplate;

    protected String id;

    protected Boolean relay;

    public SubscribeCommand() {
        this.command = "SUBSCRIBE";
    }

    public Resource getResourceTemplate() {
        return resourceTemplate;
    }

    public void setResourceTemplate(Resource resourceTemplate) {
        this.resourceTemplate = resourceTemplate;
    }

    public Boolean getRelay() {
        return relay;
    }

    public void setRelay(Boolean relay) {
        this.relay = relay;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
