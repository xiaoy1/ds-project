package EZShare.messaging;

/**
 * Created by PaulaZ on 31/3/17.
 */

import EZShare.Resource;

public class QueryCommand extends AbstractCommand {

    public QueryCommand() {
        this.command = "QUERY";
    }

    protected Boolean relay;

    protected Resource resourceTemplate;

    public Boolean getRelay() {
        return relay;
    }

    public void setRelay(Boolean relay) {
        this.relay = relay;
    }

    public Resource getResourceTemplate() {
        return resourceTemplate;
    }

    public void setResourceTemplate(Resource resourceTemplate) {
        this.resourceTemplate = resourceTemplate;
    }

}
