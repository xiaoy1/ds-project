package EZShare.messaging;

import EZShare.Resource;

/**
 * Created by shaw on 17/3/30.
 */
public class ShareCommand extends AbstractCommand{

    protected String secret;

    protected Resource resource;

    public ShareCommand() {
        this.command = "SHARE";
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

}

