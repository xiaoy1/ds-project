package EZShare.messaging;

import EZShare.Resource;

/**
 * Created by xiaoy on 16/05/17.
 */
public class SubRelayCommand extends AbstractCommand {

    public enum TYPE {SUB, RELAY, UNSUB}

    protected TYPE type;

    protected String id;
    protected ServerTemplate address;
    protected Resource resource;

    public SubRelayCommand() {
        this.command = "SUBRELAY";
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ServerTemplate getAddress() {
        return address;
    }

    public void setAddress(ServerTemplate address) {
        this.address = address;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

}
