package EZShare.messaging;

import EZShare.Resource;

/**
 * Created by PaulaZ on 31/3/17.
 */
public class FetchCommand extends AbstractCommand {

    protected Resource resourceTemplate;

    public FetchCommand() {
        this.command = "FETCH";
    }

    public Resource getResourceTemplate() {
        return resourceTemplate;
    }

    public void setResourceTemplate(Resource resourceTemplate) {
        this.resourceTemplate = resourceTemplate;
    }

}
