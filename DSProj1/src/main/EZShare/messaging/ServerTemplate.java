package EZShare.messaging;

/**
 * Created by xiaoy on 30/03/17.
 */
public class ServerTemplate {

    protected String hostname;

    protected int port;

    public ServerTemplate(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServerTemplate that = (ServerTemplate) o;

        if (port != that.port) return false;
        return hostname != null ? hostname.equals(that.hostname) : that.hostname == null;
    }

    @Override
    public int hashCode() {
        int result = hostname != null ? hostname.hashCode() : 0;
        result = 31 * result + port;
        return result;
    }

    @Override
    public String toString() {
        return "ServerTemplate{" +
                "hostname='" + hostname + '\'' +
                ", port=" + port +
                '}';
    }

}
