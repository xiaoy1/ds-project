package EZShare.messaging;

import java.util.List;

/**
 * Created by xiaoy on 30/03/17.
 */
public class ExchangeCommand extends AbstractCommand {

    protected List<ServerTemplate> serverList;

    public ExchangeCommand() {
        this.command = "EXCHANGE";
    }

    public List<ServerTemplate> getServerList() {
        return serverList;
    }

    public void setServerList(List<ServerTemplate> serverList) {
        this.serverList = serverList;
    }

}
