package EZShare;

import EZShare.messaging.*;
import EZShare.utils.JsonTrimmer;
import com.google.gson.*;
import org.apache.commons.validator.routines.DomainValidator;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.util.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by xiaoy on 25/03/17.
 */
public class ServerConnection implements Runnable {

    private static final Logger logger = LogManager.getRootLogger();

    protected  static final int DEFAULT_SOCKET_RECEIVE_TIMEOUT = 5000;

    private Socket clientSocket;
    private ServerControl serverControl;

    public ServerConnection(Socket clientSocket, ServerControl serverControl) {
        this.clientSocket = clientSocket;
        this.serverControl = serverControl;
    }

    public void run () {
        try (
                Socket socket = clientSocket;
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                DataInputStream in = new DataInputStream(socket.getInputStream())
        ) {
            serverControl.handleCommand(socket, out, in);
        } catch(IOException e) {
            logger.error(e);
        } catch (URISyntaxException e) {
            logger.error(e);
        } catch (Exception e) {
            logger.error(e);
        }
    }



}