package EZShare;

import EZShare.subscription.SubscriptionRegistry;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;

import java.io.*;

public class Server {

    protected static final int DEFAULT_EXCHANGE_INTERVAL_IN_SECONDS = 600;
    protected static final int DEFAULT_CONNECTION_INTERVAL_LIMIT_IN_SECONDS = 1;
    protected static final String DEFAULT_ADVERTISED_HOSTNAME = "localhost";
    protected static final int DEFAULT_PORT_NUMBER = 3780;
    protected static final int DEFAULT_SECURE_PORT_NUMBER = 3781;
    protected static final Logger logger = LogManager.getRootLogger();

	public static void main(String[] args) {

        logger.info("Starting the EZShare Server");

        String advertisedHostname = DEFAULT_ADVERTISED_HOSTNAME;
	    int connectionIntervalLimit = DEFAULT_CONNECTION_INTERVAL_LIMIT_IN_SECONDS;
	    int exchangeInterval = DEFAULT_EXCHANGE_INTERVAL_IN_SECONDS;
	    int portNumber = DEFAULT_PORT_NUMBER;
	    int securePortNumber = DEFAULT_SECURE_PORT_NUMBER;
	    String secret = null;

        try{
			
			Options options = new Options();
    		options.addOption("advertisedhostname",true,"advertised hostname");
			options.addOption("connectionintervallimit",true,"connection interval limit in seconds");
			options.addOption("exchangeinterval",true,"exchange interval in seconds");
			options.addOption("port",true,"server port, an integer");
            options.addOption("sport",true,"secure server port, an integer");
			options.addOption("secret",true,"secret");
			options.addOption("debug",false,"print debug information");
			options.addOption("h", "help",  false, "print help for the command.");
			
			// create the command line parser  
		    CommandLineParser parser = new DefaultParser();
		    CommandLine cmd = parser.parse(options, args); 
		    
		    if ((cmd.hasOption("h")||(cmd.hasOption("help")))) {
                System.out.println("-------------------------------------------------------------");
			    HelpFormatter formatter = new HelpFormatter();  
			    formatter.printHelp( "Server", options );
                System.out.println("-------------------------------------------------------------");
		    }

            Level level = null;
            if (cmd.hasOption("debug")){
				level = Level.DEBUG;
                logger.info("setting debug on");
            } else {
                level = Level.INFO;
            }

//            level = Level.TRACE; // TODO: remove to comment it out later

            LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
            Configuration conf = ctx.getConfiguration();
            conf.getLoggerConfig(LogManager.ROOT_LOGGER_NAME).setLevel(level);
            ctx.updateLoggers(conf);

            if (cmd.hasOption("port")) portNumber = Integer.parseInt(cmd.getOptionValue("port"));

            if (cmd.hasOption("sport")) securePortNumber = Integer.parseInt(cmd.getOptionValue("sport"));

            if (cmd.hasOption("secret")) secret = cmd.getOptionValue("secret");

            if (cmd.hasOption("advertisedhostname")) advertisedHostname = cmd.getOptionValue("advertisedhostname");

            if (cmd.hasOption("connectionintervallimit")) connectionIntervalLimit = Integer.parseInt(cmd.getOptionValue("connectionintervallimit"));

            if (cmd.hasOption("exchangeinterval")) exchangeInterval = Integer.parseInt(cmd.getOptionValue("exchangeinterval"));

        }catch(Exception e){
			logger.error("Unexpected exception:", e);
		}

        SubscriptionRegistry subscriptionRegistry = new SubscriptionRegistry();
        new Thread(subscriptionRegistry).start();

		DataCache dataCache = new DataCache(subscriptionRegistry);

        ServerControl serverControl = new ServerControl (
                advertisedHostname,
                portNumber,
                connectionIntervalLimit,
                exchangeInterval,
                secret,
                dataCache,
                false
	    );


        try {

            InputStream keystoreInput = Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("keystores/serverkeystore");
            byte[] buffer = new byte[keystoreInput.available()];
            keystoreInput.read(buffer);

            File targetFile = new File("serverkeystore");
            OutputStream outStream = new FileOutputStream(targetFile);
            outStream.write(buffer);

        } catch (IOException e) {
            logger.error(e);
        }

        System.setProperty("javax.net.ssl.keyStore", "serverkeystore");
        System.setProperty("javax.net.ssl.keyStorePassword", "password");
        System.setProperty("javax.net.ssl.trustStore", "serverkeystore");
//        System.setProperty("javax.net.debug","all");

        ServerControl secureServerControl = new ServerControl (
                advertisedHostname,
                securePortNumber,
                connectionIntervalLimit,
                exchangeInterval,
                secret,
                dataCache,
                true
        );

        ServerIO serverIO = new ServerIO(serverControl);
        ServerIO secureServerIO = new ServerIO(secureServerControl);

        new Thread(serverIO).start();
        new Thread(secureServerIO).start();

        try {
            Thread.sleep(7*24*60*60*1000); // default 7 day
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("Stopping Server");

        serverIO.stop();
        secureServerIO.stop();
        subscriptionRegistry.stop();

	}

}