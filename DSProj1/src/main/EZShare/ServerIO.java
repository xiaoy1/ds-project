package EZShare;

import EZShare.messaging.ServerTemplate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.SSLServerSocketFactory;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by xiaoy on 25/03/17.
 */
public class ServerIO implements Runnable {

    protected static final int DEFAULT_NUMBER_OF_THREADS = 10;
    protected static final Logger logger = LogManager.getRootLogger();

    protected ConcurrentHashMap<String, Instant> connectionLogs;

    protected ServerSocket serverSocket = null;
    protected boolean isStopped = false;
    protected Thread runningThread = null;
    protected ExecutorService threadPool = Executors.newFixedThreadPool(DEFAULT_NUMBER_OF_THREADS);
    protected ServerControl serverControl;

    public ServerIO(ServerControl serverControl) {
        this.serverControl = serverControl;
        connectionLogs = new ConcurrentHashMap<>();
    }

    @Override public void run() {

        synchronized (this) {
            this.runningThread = Thread.currentThread();
        }

        openServerSocket();

        // start exchanger
        ServerExchanger serverExchanger = new ServerExchanger(serverControl);
        new Thread(serverExchanger).start();

        // start request handling
        while (!isStopped()) {
            Socket clientSocket = null;
            try {

                clientSocket = this.serverSocket.accept();

                // connection control
                ServerTemplate clientAddress = new ServerTemplate(clientSocket.getInetAddress().getCanonicalHostName(), clientSocket.getPort());
                if (connectionLogs.keySet().contains(clientAddress.getHostname())) {
                    Instant lastAccessed = connectionLogs.get(clientAddress.getHostname());
                    long elapsedTime = Duration.between(lastAccessed, Instant.now()).toMillis();
                    if (elapsedTime < serverControl.getConnectionIntervalLimit()*1000) {
                        clientSocket.close();
                        logger.debug("Exceeding connection interval limit by hostname: {}", clientAddress.getHostname());
                        continue;
                    }
                }
                connectionLogs.put(clientAddress.getHostname(), Instant.now());
                logger.debug("Connected to client {}:{}", clientAddress.getHostname(), clientAddress.getPort());

            } catch (IOException e) {
                if (isStopped()) {
                    logger.info("Server Stopped.");
                    return;
                }
                logger.error("Error accepting client connection", e);
            }
            this.threadPool.execute(
                    new Thread(new ServerConnection(clientSocket, serverControl))
            );

        }

        // shutdown
        serverExchanger.getScheduledExecutor().shutdown();
        this.threadPool.shutdown();
        logger.info("Server Stopped.");
    }

    private void openServerSocket() {
        try {
            if (serverControl.isSecure()){
                this.serverSocket = SSLServerSocketFactory.getDefault().createServerSocket(serverControl.getPortNumber());
            } else {
                this.serverSocket = new ServerSocket(serverControl.getPortNumber());
            }
            logger.info("bound to port {}", serverControl.getPortNumber());
        } catch (IOException e) {
            logger.error("Cannot open port {}", serverControl.getPortNumber(), e);
        }
    }

    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    public synchronized void stop() {
        this.isStopped = true;
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            logger.error("Error closing server", e);
        }
    }


}
