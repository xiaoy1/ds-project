package EZShare.subscription;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by xiaoy on 18/05/17.
 */
public class DataInputStreamEventHandler implements Runnable {

    protected static final Logger logger = LogManager.getRootLogger();

    private DataInputStream dataInputStream;

    private BlockingQueue<String> queues;

    private boolean isStopped = false;

    public DataInputStreamEventHandler(DataInputStream dataInputStream) {
        this.dataInputStream = dataInputStream;
        queues = new LinkedBlockingQueue<>();
    }

    public void run() {
        try {
            String line = null;
            while (!isStopped) {
                if ((line = dataInputStream.readUTF()) != null){
                    queues.add(line);
                }
            }
        } catch (IOException e) {
            if (e instanceof SocketException) {
                logger.trace("Close input stream");
            } else if (!(e instanceof EOFException)) { // suppress EOFException exception
                logger.error(e);
            }
        }
    }

    public boolean available() {
        return !queues.isEmpty();
    }

    public String get() throws InterruptedException {
        return queues.take();
    }

    public void stopInputStream() {
        isStopped = false;
        try { dataInputStream.close();} catch (IOException e) {}
    }

}
