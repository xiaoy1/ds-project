package EZShare.subscription;

import EZShare.Resource;
import EZShare.ServerControl;
import EZShare.messaging.ServerTemplate;
import EZShare.messaging.SubRelayCommand;

import java.io.IOException;

/**
 * Created by xiaoy on 15/05/17.
 */
public class RelaySubscriptionHandler extends SubscriptionHandler {

    protected ServerControl serverControl;

    public RelaySubscriptionHandler(
            String id,
            Resource resourceTemplate,
            ServerTemplate subscriberAddress,
            ServerTemplate hostAddress,
            ServerControl serverControl
    ) {
        this.id = id;
        this.resourceTemplate = resourceTemplate;
        this.subscriberAddress = subscriberAddress;
        this.hostAddress = hostAddress;
        this.serverControl = serverControl;
    }

    @Override public void update(Resource resource) throws IOException {
        if (ServerControl.queryMatch(getResourceTemplate(), resource)) {

            SubRelayCommand subRelayCommand = new SubRelayCommand();
            subRelayCommand.setId(id);
            subRelayCommand.setAddress(hostAddress);
            subRelayCommand.setType(SubRelayCommand.TYPE.RELAY);

            resource = new Resource(resource);
            resource.setOwner("");
            resource.setChannel("");
            resource.setEzserver(String.format("%s:%d", hostAddress.getHostname(), hostAddress.getPort()));
            subRelayCommand.setResource(resource);

            serverControl.sendSimpleCommand(subRelayCommand, subscriberAddress);

        }

    }

    public ServerTemplate getFromAddress() {
        return hostAddress;
    }

    public void setFromAddress(ServerTemplate fromAddress) {
        this.hostAddress = fromAddress;
    }

}
