package EZShare.subscription;

import EZShare.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by xiaoy on 16/05/17.
 */
public class SubscriptionRegistry implements Runnable {

    protected static final Logger logger = LogManager.getRootLogger();

    private CopyOnWriteArrayList<SubscriptionHandler> subscriptionHandlers;
    private BlockingQueue<Resource> jobs;
    private boolean isStopped = false;

    public SubscriptionRegistry() {
        subscriptionHandlers = new CopyOnWriteArrayList<>();
        jobs = new LinkedBlockingDeque<>();
    }

    @Override public void run() {

        try {
            while (!isStopped()) {
                if (!jobs.isEmpty()) updateAll(jobs.take());
            }
        } catch (InterruptedException e) {
            logger.error(e);
        } catch (IOException e) {
            logger.error(e);
        }

    }

    public void registerHandler(SubscriptionHandler subscriptionHandler) {
        subscriptionHandlers.add(subscriptionHandler);
    }

    public void deregisterHandler(SubscriptionHandler subscriptionHandler) {
        subscriptionHandlers.remove(subscriptionHandler);
    }

    private void updateAll(Resource resource) throws IOException {
        for (SubscriptionHandler subscriptionHandler: subscriptionHandlers) {
            subscriptionHandler.update(resource);
        }
    }

    public void addJob(Resource resource) {
        jobs.add(resource);
    }

    public SubscriptionHandler findSubscriptionHandler(String id) {
        for (SubscriptionHandler subscriptionHandler: subscriptionHandlers) {
            if ((id != null) && (id.equals(subscriptionHandler.getId()))) {
                return subscriptionHandler;
            }
        }
        return null;
    }

    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    public synchronized void stop() {
        this.isStopped = true;
    }

}
