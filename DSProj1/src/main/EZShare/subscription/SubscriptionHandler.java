package EZShare.subscription;

import EZShare.Resource;
import EZShare.messaging.ServerTemplate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

/**
 * Created by xiaoy on 15/05/17.
 */
public abstract class SubscriptionHandler {

    protected static final Gson gson = new GsonBuilder().create();

    protected Resource resourceTemplate;
    protected ServerTemplate subscriberAddress;
    protected ServerTemplate hostAddress;
    protected String id;

    abstract public void update(Resource resource) throws IOException;

    public Resource getResourceTemplate() {
        return resourceTemplate;
    }

    public ServerTemplate getSubscriberAddress() {
        return subscriberAddress;
    }

    public ServerTemplate getHostAddress() {
        return hostAddress;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubscriptionHandler that = (SubscriptionHandler) o;

        if (resourceTemplate != null ? !resourceTemplate.equals(that.resourceTemplate) : that.resourceTemplate != null)
            return false;
        if (subscriberAddress != null ? !subscriberAddress.equals(that.subscriberAddress) : that.subscriberAddress != null)
            return false;
        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        int result = resourceTemplate != null ? resourceTemplate.hashCode() : 0;
        result = 31 * result + (subscriberAddress != null ? subscriberAddress.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

}