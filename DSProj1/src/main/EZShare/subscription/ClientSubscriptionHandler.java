package EZShare.subscription;

import EZShare.Resource;
import EZShare.ServerControl;
import EZShare.messaging.ServerTemplate;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * TODO: improve id generation as long can be overflowed but in such scenario though it is unlikely in such application
 *
 * Created by xiaoy on 15/05/17.
 */
public class ClientSubscriptionHandler extends SubscriptionHandler {

    protected static AtomicLong idCounter = new AtomicLong();

    private BlockingQueue<Resource> pendingResultList;
    private String clientSuppliedId;

    public ClientSubscriptionHandler(
            ServerTemplate hostAddress,
            ServerTemplate subscriberAddress,
            Resource resourceTemplate,
            String clientSuppliedId
            ) {

        this.subscriberAddress = subscriberAddress;
        this.hostAddress = hostAddress;
        this.resourceTemplate = resourceTemplate;
        this.clientSuppliedId = clientSuppliedId;
        if (clientSuppliedId == null)
            this.id = String.format("%s:%d:%d", this.subscriberAddress.getHostname(), this.subscriberAddress.getPort(), idCounter.getAndIncrement());
        else {
            this.id = String.format("%s:%d:%s", this.subscriberAddress.getHostname(), this.subscriberAddress.getPort(), this.clientSuppliedId);
        }
        pendingResultList = new LinkedBlockingQueue<>();

    }

    @Override public void update(Resource resource) throws IOException {

        if (ServerControl.queryMatch(getResourceTemplate(), resource)) {
            if ("".equals(resource.getEzserver())) { // from local data
                resource = new Resource(resource);
                if (!"".equals(resource.getOwner()))
                    resource.setOwner("*");
                resource.setEzserver(String.format("%s:%d", hostAddress.getHostname(), hostAddress.getPort()));
            }
            pendingResultList.add(resource);
        }

    }

    public Resource getPendingResult() throws InterruptedException {
        return pendingResultList.take();
    }

    public boolean isPendingResultListEmpty() throws InterruptedException {
        return pendingResultList.isEmpty();
    }

    public String getClientSuppliedId() {
        return clientSuppliedId;
    }

}
