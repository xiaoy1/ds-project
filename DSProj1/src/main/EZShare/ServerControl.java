package EZShare;

import EZShare.messaging.*;
import EZShare.subscription.ClientSubscriptionHandler;
import EZShare.subscription.DataInputStreamEventHandler;
import EZShare.subscription.RelaySubscriptionHandler;
import EZShare.subscription.SubscriptionHandler;
import EZShare.utils.JsonTrimmer;
import com.google.gson.*;
import org.apache.commons.validator.routines.DomainValidator;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import EZShare.utils.RandomString;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 *
 *
 */
public class ServerControl {
	
	private final static int DEFAULT_SECRET_STRING_LENGTH = 26;
	
	private static final Logger logger = LogManager.getRootLogger();
	private static final Gson gson = new GsonBuilder().create();

	private DataCache dataCache;

	private CopyOnWriteArraySet<ServerTemplate> serverList;

	private String advertisedHostname;
	private int portNumber;

	private int connectionIntervalLimit;
	private int exchangeInterval;

	private String secret;

	private boolean isSecure;

    private SocketFactory socketFactory;

	public ServerControl (
			String advertisedHostname,
			int portNumber,
			int connectionIntervalLimit,
			int exchangeInterval,
			String secret,
			DataCache dataCache,
            boolean isSecure
	) {

		if (secret == null) {
			this.secret = new RandomString(DEFAULT_SECRET_STRING_LENGTH).nextString();
		} else {
			this.secret = secret;
		}
		logger.info("using secret: {}", this.secret);

		this.advertisedHostname = advertisedHostname;
		logger.info("using advertised hostname: {}", this.advertisedHostname);

		this.portNumber = portNumber;
		this.connectionIntervalLimit = connectionIntervalLimit;
		this.exchangeInterval = exchangeInterval;

		this.dataCache = dataCache;

        this.isSecure = isSecure;

        this.socketFactory = isSecure == true ? SSLSocketFactory.getDefault() : SocketFactory.getDefault();

		this.serverList = new CopyOnWriteArraySet<>();
		serverList.add(new ServerTemplate(this.advertisedHostname, this.portNumber));

	}

	protected void handleCommand(Socket socket, DataOutputStream out, DataInputStream in) throws IOException, URISyntaxException {

        String requestJson = in.readUTF();

        logger.trace("Received (before trimming and correction): {}", requestJson);
		requestJson = requestJson.replaceAll("\\\\u0000", "");
		logger.trace("Received (after replacing \\0): {}", requestJson);

		JsonObject obj = null;
		try {
			obj = new JsonParser().parse(requestJson).getAsJsonObject();
		} catch (Exception e) {
			out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing or incorrect type for command")));
			logger.error(e);
			return;
		}

		// trim white spaces

		StringBuffer sb = new StringBuffer();
		JsonTrimmer.trimWhiteSpacesFromJsonObject(obj, sb);

		logger.debug("Received (after cleaning): {}", sb.toString());

		obj = new JsonParser().parse(sb.toString()).getAsJsonObject();


		if (!obj.has("command")) {
			out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing or incorrect type for command")));
			return;
		}

		switch (obj.get("command").getAsString().toUpperCase()) {

		    case "EXCHANGE":

				if (socket != null) socket.setSoTimeout(ServerConnection.DEFAULT_SOCKET_RECEIVE_TIMEOUT);
				ExchangeCommand exchangeCommand = null;
				try {
					exchangeCommand = gson.fromJson(obj, ExchangeCommand.class);
				} catch (JsonSyntaxException e) {
					out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing or incorrect type for command")));
					break;
				}
				this.handleExchangeCommand(exchangeCommand, out);

                out.flush();
                logger.debug("Close client {}:{}", socket.getInetAddress().getCanonicalHostName(), socket.getPort());

				break;

			case "FETCH":

				FetchCommand fetchCommand = null;
				try {
					fetchCommand = gson.fromJson(obj, FetchCommand.class);
				} catch (JsonSyntaxException e) {
					out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing or incorrect type for command")));
					break;
				}
				if ("*".equals(fetchCommand.getResourceTemplate().getOwner())) {
					out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "invalid resourceTemplate")));
					break;
				}
				fetchCommand.getResourceTemplate().setDefaultValues();
				this.handleFetchCommand(fetchCommand, out);

                out.flush();
                logger.debug("Close client {}:{}", socket.getInetAddress().getCanonicalHostName(), socket.getPort());

                break;

			case "PUBLISH":

				if (socket != null) socket.setSoTimeout(ServerConnection.DEFAULT_SOCKET_RECEIVE_TIMEOUT);
				PublishCommand publishCommand = null;
				try {
					publishCommand = gson.fromJson(obj, PublishCommand.class);
				} catch (JsonSyntaxException e) {
					out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing or incorrect type for command")));
					break;
				}
                Resource resource = publishCommand.getResource();
                if (resource == null) {
                    out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing resource")));
                    break;
                }
				if ("*".equals(publishCommand.getResource().getOwner())) {
					out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "invalid resourceTemplate")));
					break;
				}
				publishCommand.getResource().setDefaultValues();
				this.handlePublishCommand(publishCommand, out);

                out.flush();
                logger.debug("Close client {}:{}", socket.getInetAddress().getCanonicalHostName(), socket.getPort());

				break;

			case "QUERY":

				if (socket != null) socket.setSoTimeout(ServerConnection.DEFAULT_SOCKET_RECEIVE_TIMEOUT);
				QueryCommand queryCommand = null;
				try {
					queryCommand = gson.fromJson(obj, QueryCommand.class);
				} catch (JsonSyntaxException e) {
					out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing or incorrect type for command")));
					break;
				}
				if ("*".equals(queryCommand.getResourceTemplate().getOwner())) {
					out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "invalid resourceTemplate")));
					break;
				}
				queryCommand.getResourceTemplate().setDefaultValues();
				this.handleQueryCommand(queryCommand, out);

                out.flush();
                logger.debug("Close client {}:{}", socket.getInetAddress().getCanonicalHostName(), socket.getPort());

				break;

			case "REMOVE":

				if (socket != null) socket.setSoTimeout(ServerConnection.DEFAULT_SOCKET_RECEIVE_TIMEOUT);
				RemoveCommand removeCommand = null;
				try {
					removeCommand = gson.fromJson(obj, RemoveCommand.class);
				} catch (JsonSyntaxException e) {
					out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing or incorrect type for command")));
					break;
				}
				if ("*".equals(removeCommand.getResource().getOwner())) {
					out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "invalid resourceTemplate")));
					break;
				}
				removeCommand.getResource().setDefaultValues();
				this.handleRemoveCommand(removeCommand, out);

                out.flush();
                logger.debug("Close client {}:{}", socket.getInetAddress().getCanonicalHostName(), socket.getPort());

				break;

			case "SHARE":

				if (socket != null) socket.setSoTimeout(ServerConnection.DEFAULT_SOCKET_RECEIVE_TIMEOUT);
				ShareCommand shareCommand = null;
				try {
					shareCommand = gson.fromJson(obj, ShareCommand.class);
				} catch (JsonSyntaxException e) {
					out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing or incorrect type for command")));
					break;
				}
				if ("*".equals(shareCommand.getResource().getOwner())) {
					out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "invalid resourceTemplate")));
					break;
				}
				shareCommand.getResource().setDefaultValues();
				this.handleShareCommand(shareCommand, out);

                out.flush();
                logger.debug("Close client {}:{}", socket.getInetAddress().getCanonicalHostName(), socket.getPort());

				break;

            case "SUBSCRIBE":

                SubscribeCommand subscribeCommand = null;
                try {
                    subscribeCommand = gson.fromJson(obj, SubscribeCommand.class);
                } catch (JsonSyntaxException e) {
                    out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing or incorrect type for command")));
                    break;
                }
                Resource resourceTemplate = subscribeCommand.getResourceTemplate();
                if (resourceTemplate == null) {
                    out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing resourceTemplate")));
                    break;
                }
                if ("*".equals(subscribeCommand.getResourceTemplate().getOwner())) {
                    out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "invalid resourceTemplate")));
                    break;
                }
                subscribeCommand.getResourceTemplate().setDefaultValues();
                ServerTemplate subscriberAddress = new ServerTemplate(socket.getInetAddress().getHostAddress(), socket.getPort());
                this.handleSubscribeCommand(subscribeCommand, subscriberAddress, out, in);

                logger.debug("Close client {}:{}", subscriberAddress.getHostname(), subscriberAddress.getPort());

                break;

            case "SUBRELAY": // commands between servers

                if (socket != null) socket.setSoTimeout(ServerConnection.DEFAULT_SOCKET_RECEIVE_TIMEOUT);
                SubRelayCommand subRelayCommand = null;
                try {
                    subRelayCommand = gson.fromJson(obj, SubRelayCommand.class);
                } catch (JsonSyntaxException e) {
                    out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing or incorrect type for command")));
                    break;
                }
                this.handleSubRelayCommand(subRelayCommand, out);

                out.flush();
                logger.debug("Close client {}:{}", socket.getInetAddress().getCanonicalHostName(), socket.getPort());

                break;

            default:

				out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "invalid command")));
				logger.error("Received invalid command {}", obj.get("command").getAsString().toUpperCase());


                out.flush();
                logger.debug("Close client {}:{}", socket.getInetAddress().getCanonicalHostName(), socket.getPort());

		}

	}

	public void sendSimpleCommand(AbstractCommand command, ServerTemplate address) {

		try (
				Socket relaySocket = socketFactory.createSocket(address.getHostname(), address.getPort());
				DataOutputStream output = new DataOutputStream(relaySocket.getOutputStream());
				DataInputStream input = new DataInputStream(relaySocket.getInputStream())
		) {

            logger.debug("Connected to host {}:{}", address.getHostname(), address.getPort());

			String message = gson.toJson(command);
			output.writeUTF(message);
			logger.debug("SENT: {}", message);

			String line = null;
			while ((line = input.readUTF()) != null) {
				logger.debug("RECEIVED: {}", line);
			}

		} catch (IOException e) {
			if (!(e instanceof EOFException)) {
				logger.error("Error when connecting to {}:{}", address.getHostname(), address.getPort(), e);
			}
		}

	}

    public void broadcastSimpleCommand(AbstractCommand command, Collection<ServerTemplate> serverList) {
		for (ServerTemplate address : serverList) {
			if (advertisedHostname.equals(address.getHostname()) && portNumber == address.getPort())
				continue;
			sendSimpleCommand(command, address);
		}
	}

    protected void handleSubscribeCommand(SubscribeCommand subscribeCommand, ServerTemplate subscriberAddress, DataOutputStream out, DataInputStream in) throws IOException {

		String clientSuppliedId = subscribeCommand.getId();

        AcknowledgeTemplate ack = new AcknowledgeTemplate("success", null);
		ack.setId(clientSuppliedId);
		out.writeUTF(gson.toJson(ack));

		ClientSubscriptionHandler clientSubscriptionHandler = new ClientSubscriptionHandler(
				new ServerTemplate(advertisedHostname, portNumber),
				subscriberAddress,
				subscribeCommand.getResourceTemplate(),
                clientSuppliedId);
	    dataCache.getRegistry().registerHandler(clientSubscriptionHandler);

	    Set<ServerTemplate> serverListSet = new HashSet<>(serverList);

		SubRelayCommand subRelayCommand = null;

		if (subscribeCommand.getRelay() == true) {

			subRelayCommand = new SubRelayCommand();
			subRelayCommand.setType(SubRelayCommand.TYPE.SUB);
			subRelayCommand.setResource(subscribeCommand.getResourceTemplate());
			subRelayCommand.setAddress(clientSubscriptionHandler.getHostAddress());
			subRelayCommand.setId(clientSubscriptionHandler.getId());

			broadcastSimpleCommand(subRelayCommand, serverListSet);

		}

		String line = null;
		int counter = 0;
		boolean isStopped = false;

		DataInputStreamEventHandler inputStreamHandler = new DataInputStreamEventHandler(in);
		Thread inputThread = new Thread(inputStreamHandler);
		inputThread.start();

        try {
            while (!isStopped) {
                if (!clientSubscriptionHandler.isPendingResultListEmpty()) {
					out.writeUTF(gson.toJson(clientSubscriptionHandler.getPendingResult()));
					counter++;
                }
                if (inputStreamHandler.available()) {
                	line = inputStreamHandler.get();
                    logger.debug("RECEIVED: {}", line);
                    UnsubscribeCommand unsubscribeCommand = null;
                    try {
                        unsubscribeCommand = gson.fromJson(line, UnsubscribeCommand.class);
                        if (unsubscribeCommand != null) {
                            isStopped = true;
							continue;
                        }
                    } catch(Exception e) {
                        logger.error(e);
                    }
				}
				if (subscribeCommand.getRelay() == true && serverListSet.size() < serverList.size()) {
                	for (ServerTemplate address: serverList) {
                		if (!serverListSet.contains(address)) {
							serverListSet.add(address);
							sendSimpleCommand(subRelayCommand, address);
						}
					}
				}
            }

        }  catch (InterruptedException e) {
            logger.error(e);
        }

		if (subscribeCommand.getRelay() == true) {

			subRelayCommand = new SubRelayCommand();
			subRelayCommand.setType(SubRelayCommand.TYPE.UNSUB);
            subRelayCommand.setId(clientSubscriptionHandler.getId());

			broadcastSimpleCommand(subRelayCommand, serverListSet);

		}

		dataCache.getRegistry().deregisterHandler(clientSubscriptionHandler);

        String msg = gson.toJson(new Result(counter));
        out.writeUTF(msg);
		logger.debug("SENT: {}", msg);

		inputStreamHandler.stopInputStream();

    }

	/**
	 * TODO: add ack
	 *
	 * @param subRelayCommand
	 * @param out
	 * @throws IOException
	 */
	protected void handleSubRelayCommand(SubRelayCommand subRelayCommand, DataOutputStream out) throws IOException {

        if (subRelayCommand.getType() == SubRelayCommand.TYPE.RELAY) {

            SubscriptionHandler clientSubscriptionHandler = dataCache.getRegistry().findSubscriptionHandler(subRelayCommand.getId());
			if (clientSubscriptionHandler != null)
            	clientSubscriptionHandler.update(subRelayCommand.getResource());
			else {
				SubRelayCommand newSubRelayCommand = new SubRelayCommand();
				newSubRelayCommand.setType(SubRelayCommand.TYPE.UNSUB);
                newSubRelayCommand.setId(subRelayCommand.getId());
				sendSimpleCommand(newSubRelayCommand, subRelayCommand.getAddress());
			}

        } else if (subRelayCommand.getType() == SubRelayCommand.TYPE.SUB) {

        	dataCache.getRegistry().registerHandler(new RelaySubscriptionHandler(
						subRelayCommand.getId(),
						subRelayCommand.getResource(),
						subRelayCommand.getAddress(),
						new ServerTemplate(advertisedHostname, portNumber),
						this));

        } else if (subRelayCommand.getType() == SubRelayCommand.TYPE.UNSUB) {

			SubscriptionHandler relaySubscriptionHandler = dataCache.getRegistry().findSubscriptionHandler(subRelayCommand.getId());
			if (relaySubscriptionHandler != null)
				dataCache.getRegistry().deregisterHandler(relaySubscriptionHandler);

        } else {
        	out.writeUTF(gson.toJson(new AcknowledgeTemplate("error", "missing or incorrect type for command")));
		}

    }

	protected void handleExchangeCommand(ExchangeCommand exchangeCommand, DataOutputStream out) throws IOException {

		AcknowledgeTemplate ack;

		List<ServerTemplate> serverList = exchangeCommand.getServerList();
		if (!serverList.isEmpty()) {
			DomainValidator domainValidator =  DomainValidator.getInstance();
			if (serverList.stream().allMatch(t->domainValidator.isValid(t.getHostname()))) {
				this.serverList.addAll(serverList);
				ack = new AcknowledgeTemplate("success", null);
			} else {
				ack = new AcknowledgeTemplate("error", "missing or invalid server list");
			}
		} else {
			ack = new AcknowledgeTemplate("error", "missing resourceTemplate");
		}
		out.writeUTF(gson.toJson(ack));

	}

	public static boolean queryMatch(Resource resourceTemplate, Resource resource) {
		return ((resource.getChannel() != null && resource.getChannel().equals(resourceTemplate.getChannel())) &&
				((resource.getOwner() != null && resource.getOwner().equals(resourceTemplate.getOwner())) || "".equals(resourceTemplate.getOwner()) || resourceTemplate.getOwner() == null) &&
				(Resource.matchTags(resourceTemplate.getTags(), resource.getTags())) &&
				((resource.getUri() != null && resource.getUri().equals(resourceTemplate.getUri())) || "".equals(resourceTemplate.getUri()) || resourceTemplate.getUri() == null) &&
				(
						((resourceTemplate.getName() != null && !"".equals(resourceTemplate.getName())) && resource.getName() != null && resource.getName().contains(resourceTemplate.getName())) ||
								((resourceTemplate.getDescription() != null && !"".equals(resourceTemplate.getDescription())) && resource.getDescription() != null && resource.getDescription().contains(resourceTemplate.getDescription())) ||
								((resourceTemplate.getName() == null || "".equals(resourceTemplate.getName())) && (resourceTemplate.getDescription() == null || "".equals(resourceTemplate.getDescription())))
				));
	}

	protected void handleQueryCommand(QueryCommand queryCommand, DataOutputStream out) throws IOException {

		AcknowledgeTemplate ack;

		Resource resource = queryCommand.getResourceTemplate();

		boolean getAll = resource.isEmpty();

		List<Resource> resultList = new ArrayList<>();
		for (Resource r : dataCache) {
			if (getAll || queryMatch(resource, r)) {
				Resource temp = new Resource(r);
				if (!"".equals(temp.getOwner())) temp.setOwner("*");
				temp.setEzserver(this.advertisedHostname + ":" + this.portNumber);
				resultList.add(temp);
			}
		}

		if (queryCommand.getRelay()) {
			for (ServerTemplate s : this.serverList) {
				if (advertisedHostname.equals(s.getHostname()) &&
						portNumber == s.getPort())
					continue;
				try (
						Socket relaySocket = socketFactory.createSocket(s.getHostname(), s.getPort());
						DataOutputStream output = new DataOutputStream(relaySocket.getOutputStream());
						DataInputStream input = new DataInputStream(relaySocket.getInputStream())
				) {
					resource.setOwner("");
					resource.setChannel("");
					queryCommand.setResourceTemplate(resource);
					queryCommand.setRelay(false);
					output.writeUTF(gson.toJson(queryCommand));
					logger.debug("SENT: {}", gson.toJson(queryCommand));
					String line = input.readUTF();
					AcknowledgeTemplate relayAck = gson.fromJson(line, AcknowledgeTemplate.class);
					if ("success".equals(relayAck.getResponse().toLowerCase())) {
						while ((line = input.readUTF()) != null) {
							logger.debug("RECEIVED: {}", line);
							JsonObject jsonObject = new JsonParser().parse(line).getAsJsonObject();
							if (jsonObject.has("resultSize")) {
								continue;
							}
							resultList.add(gson.fromJson(line, Resource.class));
						}
					}
				} catch (IOException e) {
					if (!(e instanceof EOFException)) {
						logger.error("Error when connecting to {}:{}", s.getHostname(), s.getPort(), e);
					}
				}
			}
		}

		ack = new AcknowledgeTemplate("success", null);
		out.writeUTF(gson.toJson(ack));
		for (Resource element : resultList) {
			out.writeUTF(gson.toJson(element));
		}
		int resultSize = resultList.size();
		out.writeUTF(gson.toJson(new Result(resultSize)));

	}

	protected  void handleShareCommand(ShareCommand shareCommand, DataOutputStream out) throws IOException {

		AcknowledgeTemplate ack = null;

		Resource resource = shareCommand.getResource();

		boolean matchedChannelAndUriButOwner = false;

		if (resource != null && shareCommand.getSecret() != null) {
			if (!shareCommand.getSecret().equals(this.secret)) {
				ack = new AcknowledgeTemplate("error", "incorrect secret");
			} else if (!"".equals(resource.getUri()) && resource.getUri().startsWith("file:")) {
				for (Resource r : dataCache) {
					if (r.getChannel().equals(resource.getChannel()) &&
							r.getUri().equals(resource.getUri()) &&
							!r.getOwner().equals(resource.getOwner())) {
						matchedChannelAndUriButOwner = true;
						break;
					}
				}
				if (matchedChannelAndUriButOwner) {
					ack = new AcknowledgeTemplate("error", "cannot share resourceTemplate");
				} else {
					dataCache.add(resource);
				}
				if (dataCache.contains(resource)) {
					ack = new AcknowledgeTemplate("success", null);
				}
			} else {
				ack = new AcknowledgeTemplate("error", "cannot share resourceTemplate");
			}

		} else {
			ack = new AcknowledgeTemplate("error", "missing resourceTemplate and/or secret");
		}

		out.writeUTF(gson.toJson(ack));

	}

	protected  void handlePublishCommand(PublishCommand publishCommand, DataOutputStream out) throws IOException {

		AcknowledgeTemplate ack = null;

		Resource resource = publishCommand.getResource();

		boolean matchedChannelAndUriButOwner = false;

		UrlValidator urlValidator = new UrlValidator();

		if (resource != null) {
			if (!"".equals(resource.getUri()) && urlValidator.isValid(resource.getUri()) &&
					!resource.getUri().startsWith("file:")) {
				for (Resource r : dataCache) {
					if (r.getChannel().equals(resource.getChannel()) &&
							r.getUri().equals(resource.getUri()) &&
							!r.getOwner().equals(resource.getOwner())) {
						matchedChannelAndUriButOwner = true;
						break;
					}
				}
				if (matchedChannelAndUriButOwner) {
					ack = new AcknowledgeTemplate("error","cannot publish resourceTemplate");
				}
				else {
					dataCache.add(resource);
				}
				if (dataCache.contains(resource)) {
					ack = new AcknowledgeTemplate("success", null);
				}
			} else {
				ack = new AcknowledgeTemplate("error","cannot publish resourceTemplate");
			}
		} else {
			ack = new AcknowledgeTemplate("error","missing resourceTemplate");
		}

		out.writeUTF(gson.toJson(ack));

	}

	protected void handleRemoveCommand(RemoveCommand removeCommand, DataOutputStream out) throws IOException {

		Resource resource = removeCommand.getResource();

		AcknowledgeTemplate ack = null;
		if (resource == null) {
			ack = new AcknowledgeTemplate("error", "missing resourceTemplate");
		} else if (!dataCache.contains(resource)) {
			ack = new AcknowledgeTemplate("error", "cannot remove resourceTemplate");
		} else if (dataCache.remove(resource)) {
			ack = new AcknowledgeTemplate("success", null);
		}
		out.writeUTF(gson.toJson(ack));

	}

	protected void handleFetchCommand(FetchCommand fetchCommand, DataOutputStream out) throws IOException, URISyntaxException {

		AcknowledgeTemplate ack;

		Resource queryResource = fetchCommand.getResourceTemplate();

		if (queryResource == null) {
			ack  = new AcknowledgeTemplate("error","missing resourceTemplate");
			out.writeUTF(gson.toJson(ack));
		} else if (!queryResource.getUri().contains("file://")){
			ack = new AcknowledgeTemplate("error","invalid resourceTemplate template");
			out.writeUTF(gson.toJson(ack));
		} else {

			Optional<Resource> optionalResource = dataCache.stream().filter(t->t.compareUriAndChannel(queryResource)).findFirst();
			if (optionalResource.isPresent()) {

				ack = new AcknowledgeTemplate("success", null);
				out.writeUTF(gson.toJson(ack));

				Resource matchedResource = optionalResource.get();
				Resource temp = new Resource(matchedResource);
				if (!"".equals(temp.getOwner())) temp.setOwner("*");
				URI uri = new URL(temp.getUri()).toURI();
				if(uri.getAuthority() != null && uri.getAuthority().length() > 0) {
					uri = (new URL("file://" + temp.getUri().substring("file:".length()))).toURI();
				}

				File file = new File(uri);

				temp.setResourceSize(file.length());
				temp.setEzserver(this.advertisedHostname);
				out.writeUTF(gson.toJson(temp));

				try (FileInputStream inputStream = new FileInputStream(file)) {
//					int bufferSize = clientSocket.getSendBufferSize();
					int bufferSize = 8192;
					byte[] buffer = new byte[bufferSize];
					long bytesRemaining = file.length();
					int num = (int) Math.min(bufferSize, bytesRemaining);
					while (bytesRemaining > 0) {
						inputStream.read(buffer, 0, num);
						out.write(buffer, 0, num);
						bytesRemaining -= num;
						num = (int) Math.min(bufferSize, bytesRemaining);
					}
				}

				out.writeUTF(gson.toJson(new Result(1)));

			} else {
				ack = new AcknowledgeTemplate("success", null);
				out.writeUTF(gson.toJson(ack));
				out.writeUTF(gson.toJson(new Result(0)));
			}

		}

	}

	public CopyOnWriteArraySet<ServerTemplate> getServerList() {
		return serverList;
	}

	public String getAdvertisedHostname() {
		return advertisedHostname;
	}

	public int getPortNumber() {
		return portNumber;
	}

	public int getConnectionIntervalLimit() {
		return connectionIntervalLimit;
	}

	public int getExchangeInterval() {
		return exchangeInterval;
	}

	public String getSecret() {
		return secret;
	}

	public boolean isSecure() {
		return isSecure;
	}

    public SocketFactory getSocketFactory() {
        return socketFactory;
    }
}
