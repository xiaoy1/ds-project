package EZShare;

import EZShare.messaging.ExchangeCommand;
import EZShare.messaging.ServerTemplate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ServerExchanger implements Runnable {

    protected static final Logger logger = LogManager.getRootLogger();
    protected static final Gson gson = new GsonBuilder().create();

    private ScheduledThreadPoolExecutor scheduledExecutor;

    private ServerControl serverControl;

    public ServerExchanger(ServerControl serverControl) {
        this.serverControl = serverControl;
        scheduledExecutor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1);
    }

    public void run() {

        logger.info("Start exchange.");
        scheduledExecutor.scheduleAtFixedRate(new Exchanger(), 0, serverControl.getExchangeInterval(), TimeUnit.SECONDS);

    }

    private class Exchanger implements Runnable {

        public void run() {

            ServerTemplate[] serverArray = serverControl.getServerList().toArray(new ServerTemplate[0]);

            if (serverArray.length == 1) return;

            ServerTemplate pickedServer = serverArray[new Random().nextInt(serverArray.length)];
            while (serverControl.getAdvertisedHostname().equals(pickedServer.getHostname()) &&
                    serverControl.getPortNumber() == pickedServer.getPort()) {
                pickedServer = serverArray[new Random().nextInt(serverArray.length)];
            }

            logger.debug("Select {}:{}", pickedServer.getHostname(), pickedServer.getPort());

            ExchangeCommand exchangeCommand = new ExchangeCommand();
            exchangeCommand.setServerList(new ArrayList<>(Arrays.asList(serverArray)));

            serverControl.sendSimpleCommand(exchangeCommand, pickedServer);

        }
    }

    public ScheduledThreadPoolExecutor getScheduledExecutor() {
        return scheduledExecutor;
    }

}
