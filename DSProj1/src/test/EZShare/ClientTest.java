package EZShare;

import org.junit.Test;

/**
 *
 */
public class ClientTest {

	/**
	 * server: sunrise.cis.unimelb.edu.au (128.250.22.135), port: 3780
	 */
//	@Test
	public void testAgainstAronServer() {

		// sample JSON
//		String[] args = {"-secure", "-publish", "-name", "Bloomberg website", "-description", "The main page for Bloomberg", "-uri", "https://www.bloomberg.com/middleeast", "-tags", "web,html", "-debug"};

		// test publish
//		String[] args = {"-host", "sunrise.cis.unimelb.edu.au", "-port", "3780", "-publish", "-name", "Bloomberg website", "-description", "The main page for Bloomberg", "-uri", "https://www.bloomberg.com/middleeast", "-tags", "web,html", "-debug"};

		// test remove
//		String[] args = {"-host", "sunrise.cis.unimelb.edu.au", "-port", "3780", "-remove", "-name", "Baidu", "-description", "The main page for Baidu", "-uri", "http://www.baidu.com", "-tags", "web,html", "-debug"};

		// test query
		String[] args = {"-query", "-debug", "-secure"};

		// test fetch
//		String[] args  = {"-debug","-fetch","-uri","file:///usr/local/share/ezshare/photo.jpg","-host", "sunrise.cis.unimelb.edu.au", "-port", "3780", "-debug"};

		// test share
//		String[] args = {"-host", "sunrise.cis.unimelb.edu.au", "-port", "3780", "-share","-secret","2os41f58vkd9e1q4ua6ov5emlv", "-name", "testfile", "-description", "a test file", "-uri", "file:///Users/shaw/shaw/note.txt", "-tags", "txt,file", "-debug"};

		// test exchange
//		String[] args = {"-host", "sunrise.cis.unimelb.edu.au", "-port", "3780", "-exchange", "-servers", "115.146.85.165:3780,115.146.85.24:3780", "-debug"};

		Client.main(args);

	}

	@Test
	public void testLocalServer() {

		// test publish
		String[] args = {"-publish", "-name", "Bloomberg website", "-description", "The main page for Bloomberg", "-uri", "https://www.bloomberg.com/middleeast", "-tags", "web,html", "-debug"};

		// test query (cannot enter when testing)
//		String[] args = {"-query", "-debug", "-secure"};

		Client.main(args);

	}

//	@Test
	public void testXYServer() {

		// test exchange
//		String[] args = {"-host", "ec2-52-26-229-33.us-west-2.compute.amazonaws.com", "-port", "3780", "-exchange", "-servers", "ec2-52-14-233-196.us-east-2.compute.amazonaws.com:3780", "-debug"};

		String[] args = {"-secure", "-host", "ec2-52-26-229-33.us-west-2.compute.amazonaws.com", "-port", "3781", "-exchange", "-servers", "ec2-52-14-233-196.us-east-2.compute.amazonaws.com:3781", "-debug"};

		Client.main(args);

	}

}