//package EZShare;
//
//import EZShare.messaging.AcknowledgeTemplate;
//import EZShare.messaging.PublishCommand;
//import EZShare.messaging.QueryCommand;
//import EZShare.messaging.Result;
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//import edu.stanford.nlp.io.StringOutputStream;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.junit.Before;
//import org.junit.FixMethodOrder;
//import org.junit.Test;
//import org.junit.runners.MethodSorters;
//
//import java.io.DataOutputStream;
//import java.io.IOException;
//import java.net.URISyntaxException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Optional;
//
//@FixMethodOrder(MethodSorters.JVM)
///**
// * Created by xiaoy on 6/05/17.
// */
//public class ServerControlTest {
//
//    private static final Logger logger = LogManager.getRootLogger();
//    private static final Gson gson = new GsonBuilder().create();
//
//    private EZShare.ServerControl serverControl;
//    private Resource resourceTemplate;
//    private StringOutputStream stringOutput;
//    private DataOutputStream out;
//
//    @Before
//    public void setUp() throws Exception {
//
//        logger.debug("Start setUp");
//
//        serverControl = new ServerControl(
//                "localhost",
//                3870,
//                1,
//                600,
//                "tortina"
//        );
//        resourceTemplate = new Resource();
//        resourceTemplate.setName("Bloomberg website");
//        resourceTemplate.setDescription("The main page for Bloomberg");
//        resourceTemplate.setUri("https://www.bloomberg.com/middleeast");
//        resourceTemplate.setDefaultValues();
//        stringOutput = new StringOutputStream();
//        out = new DataOutputStream(stringOutput);
//
//        logger.debug("End setUp");
//
//    }
//
//    @Test
//    public void testRemoveLeadingAndTrailingBlanks() throws IOException, URISyntaxException {
//
//        logger.debug("Start testRemoveLeadingAndTrailingBlanks");
//
//        Resource resourceTemplate = new Resource(resourceTemplate);
//        resourceTemplate.setTags("  web  ", " html");
//        PublishCommand publishCommand = new PublishCommand();
//        publishCommand.setResourceTemplate(resourceTemplate);
//        serverControl.handleCommand(gson.toJson(publishCommand), out, null);
//        Optional<Resource> resourceOptional = serverControl.getResources().stream().filter(r -> resourceTemplate.equals(r)).findFirst();
//        if (resourceOptional.isPresent()) {
//            org.junit.Assert.assertTrue(resourceOptional.get().getTags().containsAll(Arrays.asList("web", "html")));
//        } else {
//            org.junit.Assert.fail("Cannot find resourceTemplate");
//        }
//
//        logger.debug("End testRemoveLeadingAndTrailingBlanks");
//
//    }
//
//    @Test
//    public void testRemoveForwardSlashZero() throws IOException, URISyntaxException {
//
//        logger.debug("Start testRemoveForwardSlashZero");
//
//        Resource resourceTemplate = new Resource(resourceTemplate);
//        resourceTemplate.setName("Bloombe\0rg website");
//        PublishCommand publishCommand = new PublishCommand();
//        publishCommand.setResourceTemplate(resourceTemplate);
//        serverControl.handleCommand(gson.toJson(publishCommand), out, null);
//        Optional<Resource> resourceOptional = serverControl.getResources().stream().filter(r -> resourceTemplate.equals(r)).findFirst();
//        if (resourceOptional.isPresent()) {
//            org.junit.Assert.assertTrue("Bloomberg website".equals(resourceOptional.get().getName()));
//        } else {
//            org.junit.Assert.fail("Cannot find resourceTemplate");
//        }
//
//        logger.debug("End testRemoveForwardSlashZero");
//
//    }
//
//    @Test
//    public void testHandleQueryCommand() throws IOException, URISyntaxException {
//
//        logger.debug("Start testHandleQueryCommand");
//
//        Resource resourceTemplate = new Resource(resourceTemplate);
//        resourceTemplate.setTags("html", "web");
//        PublishCommand publishCommand = new PublishCommand();
//        publishCommand.setResourceTemplate(resourceTemplate);
//        serverControl.handleCommand(gson.toJson(publishCommand), out, null);
//
//        resourceTemplate = new Resource(resourceTemplate);
//        resourceTemplate.setName("IBM webpage");
//        resourceTemplate.setUri("https://www.ibm.com/au-en/");
//        resourceTemplate.setDescription("IBM's website");
//        publishCommand = new PublishCommand();
//        publishCommand.setResourceTemplate(resourceTemplate);
//        serverControl.handleCommand(gson.toJson(publishCommand), out, null);
//
//        QueryCommand queryCommand = new QueryCommand();
//        queryCommand.setRelay(false);
//        resourceTemplate = new Resource();
//        resourceTemplate.setTags("html");
//        queryCommand.setResourceTemplate(resourceTemplate);
//        serverControl.handleCommand(gson.toJson(queryCommand), out, null);
//
//        List<String> repliedMessages = parseUnicodeSentense(stringOutput.toString());
//        logger.debug("Replied messages: {}", repliedMessages.toString());
//
//        org.junit.Assert.assertEquals(gson.fromJson(repliedMessages.get(repliedMessages.size()-1), Result.class).getResultSize(), 1);
//
//        logger.debug("End testHandleQueryCommand");
//
//    }
//
//    @Test
//    public void testHandlePublishOverwriteCommand() throws IOException, URISyntaxException {
//
//        logger.debug("Start testHandlePublishCommand");
//
//        Resource resourceTemplate = new Resource(resourceTemplate);
//        resourceTemplate.setChannel("a");
//        resourceTemplate.setOwner("Tom");
//        resourceTemplate.setName("Baidu");
//        resourceTemplate.setDescription("Baidu main page");
//        resourceTemplate.setUri("https://www.baidu.com");
//        resourceTemplate.setTags("search engine", "Chine", "good");
//        PublishCommand publishCommand = new PublishCommand();
//        publishCommand.setResourceTemplate(resourceTemplate);
//        serverControl.handleCommand(gson.toJson(publishCommand), out, null);
//
//        resourceTemplate.setChannel("a");
//        resourceTemplate.setOwner("Tom");
//        resourceTemplate.setName("Chinese Baidu");
//        resourceTemplate.setDescription("Main page of Baidu");
//        resourceTemplate.setUri("https://www.baidu.com");
//        resourceTemplate.setTags("Chinese search engine", "popular");
//        publishCommand.setResourceTemplate(resourceTemplate);
//        serverControl.handleCommand(gson.toJson(publishCommand), out, null);
//
//        resourceTemplate.setOwner("Jim");
//        publishCommand.setResourceTemplate(resourceTemplate);
//        serverControl.handleCommand(gson.toJson(publishCommand), out, null);
//
//        List<String> repliedMessages = parseUnicodeSentense(stringOutput.toString());
//        logger.debug("Replied messages: {}", repliedMessages.toString());
//
//        org.junit.Assert.assertTrue("error".equals(gson.fromJson(repliedMessages.get(2), AcknowledgeTemplate.class).getResponse()));
//
//        QueryCommand queryCommand = new QueryCommand();
//        queryCommand.setRelay(false);
//        resourceTemplate = new Resource();
//        queryCommand.setResourceTemplate(resourceTemplate);
//        serverControl.handleCommand(gson.toJson(queryCommand), out, null);
//
//        repliedMessages = parseUnicodeSentense(stringOutput.toString());
//        logger.debug("Replied messages: {}", repliedMessages.toString());
//
//        org.junit.Assert.assertTrue("Chinese Baidu".equals(gson.fromJson(repliedMessages.get(4), Resource.class).getName()));
//
//        logger.debug("End testHandleQueryCommand");
//
//    }
//
//    @Test
//    public void testHandleQueryCommandWithEmptyResourceTemplate() throws IOException, URISyntaxException{
//        logger.debug("Start testHandleQueryCommand2");
//
//        Resource resourceTemplate = new Resource(resourceTemplate);
//        PublishCommand publishCommand = new PublishCommand();
//        publishCommand.setResourceTemplate(resourceTemplate);
//        serverControl.handleCommand(gson.toJson(publishCommand), out, null);
//
//        Resource resource2 = new Resource(resourceTemplate);
//        resource2.setName("abc");
//        resource2.setUri("https://www.abc.com");
//        resource2.setChannel("ez");
//        resource2.setOwner("ez");
//        publishCommand.setResourceTemplate(resource2);
//        serverControl.handleCommand(gson.toJson(publishCommand),out,null);
//
//        QueryCommand queryCommand = new QueryCommand();
//        queryCommand.setRelay(false);
//        Resource r = new Resource();
//        queryCommand.setResourceTemplate(r);
//        serverControl.handleCommand(gson.toJson(queryCommand),out,null);
//
//        List<String> repliedMessages = parseUnicodeSentense(stringOutput.toString());
//        logger.debug("Replied messages: {}", repliedMessages.toString());
//
//        org.junit.Assert.assertEquals(gson.fromJson(repliedMessages.get(repliedMessages.size()-1), Result.class).getResultSize(), 2);
//
//        logger.debug("End testHandleQueryCommand");
//    }
//
//    private List<String> parseUnicodeSentense(String unicodeSentence) {
//
//        List<String> results = new ArrayList<>();
//        StringBuilder sb = null;
//        final int length = unicodeSentence.length();
//        for (int offset = 0; offset < length; ) {
//
//            int codepoint = unicodeSentence.codePointAt(offset);
//            if (codepoint == 0) {
//                offset += Character.charCount(codepoint);
//                codepoint = unicodeSentence.codePointAt(offset);
//                offset += Character.charCount(codepoint);
//                if (sb == null) sb = new StringBuilder();
//                else {
//                    results.add(sb.toString());
//                    sb = new StringBuilder();
//                }
//                continue;
//            }
//            String character = String.valueOf(Character.toChars(codepoint));
//            sb.append(character);
//
//            offset += Character.charCount(codepoint);
//        }
//        if (sb != null) results.add(sb.toString());
//
//        return results;
//    }
//
//}
